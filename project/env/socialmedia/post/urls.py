from django.conf.urls import url

import post.views
#appname.views

urlpatterns = [
	url(r'^get_all_posts/', post.views.get_all_posts, name='getAllPosts'),
	url(r'^insert_post/', post.views.insert_post, name ='insertPost'),
]	