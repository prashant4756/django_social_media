# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from author.models import Author
# Create your models here.
class Post(models.Model):
    post_author = models.ForeignKey(Author, on_delete=models.CASCADE)
    message = models.CharField(max_length=200)
    post_time = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.message