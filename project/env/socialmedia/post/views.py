# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import HttpResponse


from author.views import get_user,get_author_from_username
from .models import Post

# import author
# from author.models import Author

from .serializers import PostSerializer
# Create your views here.

import json

@csrf_exempt
def insert_post(request):
	try:
		if request.method == 'POST':
			data = json.loads(request.body)
			posted_username = data['username']
			posted_message = data['message']
			if request.session['username'] is not None and request.session['username'] == posted_username:
				author = get_author_from_username(posted_username)
				if author is not None:
					post = Post()
					post.post_author = author
					post.message = posted_message
					post.save()
					return HttpResponse(json.dumps({'message':"inserted",'status': 200}))
			else:
				return HttpResponse(json.dumps({'message':"session is expired", 'status':200}))
	except Exception,e:
		print e.message
		return HttpResponse(json.dumps({'message':"unable to insert",'status':201}))

@csrf_exempt
def get_all_posts(request):
	#print request
	try:
		print "inside try"
		if request.method == 'POST':
			data = json.loads(request.body)
			posted_username = data['username']
			print posted_username
			if request.session['username'] is not None and request.session['username'] == posted_username:
				print "inside session check"
				author = get_author_from_username(posted_username)
				if author is not None:
					print "user is not none" + author.author_user.username
					posts = Post.objects.filter(post_author = author)
					if posts is None:
						print "post is not none"
						return HttpResponse(json.dumps({'message': "Entry not found", 'status': 410}))
					return HttpResponse(json.dumps({'all_posts': PostSerializer(posts, many=True).data,
                    	                    'status': 200}))
				else:
					return HttpResponse(json.dumps({'message' : "user is null" ,'status': 410}))
			else:
				return HttpResponse(json.dumps({'message':"session expired", 'status':410}))
	except Exception, e:
		print e.message
		return HttpResponse(json.dumps({'message':"something went wrong",'status':410}))
