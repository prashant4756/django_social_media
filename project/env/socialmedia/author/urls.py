from django.conf.urls import url

import author.views
#appname.views

urlpatterns = [
	url(r'^register_user/', author.views.register_user, name='registerUser'),
	url(r'^login_user/', author.views.login_user, name='loginUser'),
]