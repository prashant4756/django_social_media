from rest_framework import serializers
from .models import Author

from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'username')


class AuthorSerializer(serializers.ModelSerializer):
    author_user = UserSerializer(read_only=True)
    class Meta:
        model = Author
        fields = ('id','author_user')
