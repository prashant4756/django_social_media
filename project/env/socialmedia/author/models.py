# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.admin import User
# Create your models here.
from django.db.models import Q
class Author(models.Model):
    author_user = models.OneToOneField(User, on_delete=models.CASCADE)
    author_phone = models.CharField(max_length=13,null=True)

    def __unicode__(self):
        return self.author_user.username

    # def get_friends(self):
    #     return Friendship.objects.filter(Q(creator = self.author_user) | Q(friend = self.author_user))