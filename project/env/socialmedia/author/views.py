# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
import json
from django.contrib.auth.models import User
from django.shortcuts import HttpResponse
from .models import Author
from django.contrib.auth import authenticate
# Create your views here.

@csrf_exempt
def login_user(request):
    try:
        if request.method == 'POST':
            data = json.loads(request.body)
            posted_username = data['username']
            posted_password = data['password']
            #temp_user = get_user(posted_phone=posted_username)
            user = authenticate(username = posted_username, password = posted_password)
            if user is not None:
                request.session['username'] = posted_username
                print "session : "+request.session['username']
                return HttpResponse(json.dumps({'message':"login successful", 'status':200}))
            else:
                return HttpResponse(json.dumps({'message':"login unsuccessful", 'status':200}))

    except Exception,e:
        return HttpResponse(json.dumps({'message':"somthing went wrong", 'status':410}))

@csrf_exempt
def register_user(request):
    try:
        print "inside try"
        if request.method == 'POST':
            data = json.loads(request.body)
            posted_phone = data['phone']
            print posted_phone
            posted_name = data['name']
            print posted_name
            posted_password = data['password']
            print posted_password
            temp_user = get_user(posted_phone)
            #print "name : "+temp_user.username
            #above line raises exception if user is None
            if temp_user is None:
                print "inside creating new user"
                new_user = User.objects.create_user(username=posted_phone, password=posted_password, first_name=posted_name)
                print "new user: " + new_user.username
                if new_user is not None:
                    new_author = Author(author_user =new_user, author_phone = posted_phone)
                    new_author.save()
                    return HttpResponse(json.dumps({'message':"User added successfully",'status':200}))
            else:
                return HttpResponse(json.dumps({'message':"we already have that user registered in our database",'status':200}))


    except Exception, e:
        return HttpResponse(json.dumps({'message': "Registration Error", "status": 410}))

def get_user(posted_phone):
    print "inside get user in author view"
    user = None
    try:
        user = User.objects.get(username=posted_phone)
        return user
    except Exception,e:
        return user

def get_author_from_username(posted_phone):
    author = None
    try:
        user = get_user(posted_phone)
        author = Author.objects.get(author_user = user)
        return author
    except Exception,e:
        return author