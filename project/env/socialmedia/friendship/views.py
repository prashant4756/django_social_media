# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json

from author.views import get_user,get_author_from_username
from .models import Friendship
from django.db.models import Q
from .serializers import FriendshipSerializer
# Create your views here.

@csrf_exempt
def add_friend(request):
    try:
        if request.method == 'POST':
            data = json.loads(request.body)
            posted_creator = data['creator']
            posted_friend = data['friend']

            creator_author = get_author_from_username(posted_creator)
            friend_author = get_author_from_username(posted_friend)
            print posted_creator+" and "+posted_friend
            if request.session['username'] is not None and request.session['username'] == posted_creator:
                if creator_author.author_user.username ==friend_author.author_user.username:
                    return HttpResponse(json.dumps({'message':'Cannot send request to self', 'status':200}))
                if creator_author is not None and friend_author is not None:
                    check_already_friends = Friendship.objects.filter(creator = creator_author.author_user, friend = friend_author.author_user)
                    if check_already_friends is not None:
                        return HttpResponse(json.dumps({'message':'Already a friend', 'status':200}))

                    friendship = Friendship()
                    if friendship is not None:
                        friendship.creator = creator_author.author_user
                        friendship.friend = friend_author.author_user
                        friendship.save()
                        return HttpResponse(json.dumps({'message':'friend added', 'status':200}))
                    else:
                        return HttpResponse(json.dumps({'message':'friend cant be added added', 'status':200}))
                else:
                    return HttpResponse(json.dumps({'message':'friend cant be added/user not found', 'status':200}))
            else:
                return HttpResponse(json.dumps({'message':'session expired', 'status':200}))

        else:
            return HttpResponse(json.dumps({'message':'requested method is illegal', 'status':200}))
    except Exception,e:
        print e.message
        return HttpResponse(json.dumps({'message':'something went wrong', 'status':410}))

@csrf_exempt
def show_my_friends(request):
    try:
        if request.method == 'POST':
            data = json.loads(request.body)
            posted_user = data['username']
            print posted_user
            user = get_user(posted_user)
            print "after get_user : " +user.username
            if user is None:
                return HttpResponse(json.dumps({'message':'user not found', 'status':200}))
            if request.session['username'] is not None and request.session['username'] == posted_user:
                print "now finding friends"
                friendship = Friendship.objects.filter(Q(creator = user)| Q(friend = user))
                return HttpResponse(json.dumps({'all_friends':FriendshipSerializer(friendship, many= True).data,
                                                'message':'friends found', 'status':200}))

            else:
                return HttpResponse(json.dumps({'message':'session expired', 'status':200}))

        else:
            return HttpResponse(json.dumps({'message':'requested method is illegal', 'status':200}))


    except Exception,e:
        print e.message
        return HttpResponse(json.dumps({'message':'something went wrong', 'status':410}))

