# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.admin import User

# Create your models here.
class Friendship(models.Model):
    creator = models.ForeignKey(User, related_name='creator')
    friend = models.ForeignKey(User, related_name= 'friends_set')
    creation_date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.creator.username +" " + self.friend.username