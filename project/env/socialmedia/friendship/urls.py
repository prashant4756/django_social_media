from django.conf.urls import url

import friendship.views

urlpatterns = [
	url(r'^add_friend/', friendship.views.add_friend, name='addFriend'),
    url(r'^show_my_friends/', friendship.views.show_my_friends, name='showMyFriends'),

]